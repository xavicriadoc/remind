//
//  TaskCell.swift
//  reMIND
//
//  Created by Xavier Criado Carmona on 19/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    
    @IBOutlet weak var imgTask : UIImageView!
    @IBOutlet weak var lblTask : UILabel!
    

}

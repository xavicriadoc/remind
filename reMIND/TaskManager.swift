//
//  TaskManager.swift
//  reMIND
//
//  Created by Xavier Criado Carmona on 22/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

import Foundation


class TaskManager{
    
    static let sharedInstance = TaskManager()
    
    var tasks : [[String:String]] = [[String:String]]()
    
    func save(){
        UserDefaults.standard.set(tasks, forKey: "tasks")
    }
    
    func load(){
        if let array = UserDefaults.standard.array(forKey: "tasks") as? [[String:String]]{
            tasks = array
        }
    }
}

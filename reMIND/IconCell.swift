//
//  IconCell.swift
//  reMIND
//
//  Created by Xavier Criado Carmona on 19/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

import UIKit

class IconCell: UICollectionViewCell {
    
    @IBOutlet weak var icon : UIImageView!
    @IBOutlet weak var title : UILabel!
    
    
}
